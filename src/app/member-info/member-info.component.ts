import { GymService } from './../services/gym.service';
import { ActivatedRoute, Params } from '@angular/router';
import { MemberService } from './../services/member.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-member-info',
  templateUrl: './member-info.component.html',
  styleUrls: ['./member-info.component.css']
})
export class MemberInfoComponent implements OnInit {
  paramSubs:Subscription;
  msub:Subscription;
  gsub: Subscription;
  member;
  gyms;
  constructor(private mService: MemberService,private gService: GymService ,private route:ActivatedRoute) {
    this.member = null;
    this.gyms = null;
  }

  ngOnInit() {

    let memberId = null;
    this.paramSubs = this.route.params.subscribe(params => {
      memberId = params.memberId;
    })
    this.msub = this.mService.getMember(memberId).subscribe(res => {
      this.member = res;      
    })
    this.gsub = this.gService.getGyms().subscribe(res => {
      this.gyms = res;
    });

  }

  getGym(gymId){
    if(this.gyms){
      let gym = this.gyms.find(gyme => gyme.id == gymId);
      return gym? gym.name:'';
    }
  }

  ngOnDestroy(){
    this.paramSubs.unsubscribe()
    this.msub.unsubscribe();
    this.gsub.unsubscribe();
  }

}

import { MemberService } from './../services/member.service';
import { GymService } from './../services/gym.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-gym-info',
  templateUrl: './gym-info.component.html',
  styleUrls: ['./gym-info.component.css']
})
export class GymInfoComponent implements OnInit {
  subscription: Subscription;
  subscriptionGym: Subscription;
  subscriptionMembers: Subscription;
  gym;
  gymId;
  members;
  constructor(private router: Router, private route: ActivatedRoute, private gymService: GymService, private memberService:MemberService) {
    this.gym = null;
    this.gymId = null;
    this.members = [];
  }

  ngOnInit() {
    //Take the id from params
    this.subscription = this.route.params.subscribe(params => {
      this.gymId = params.gymId;
    });

    //get the Gym info
    this.subscriptionGym = this.gymService.getGym(this.gymId).subscribe(res => {
      this.gym = res;
    });

    //get the members of gym
    this.subscriptionMembers = this.memberService.getMembersOfGym(this.gymId).subscribe(res =>{
      this.members = res;
      console.log(this.members)
    });
  }

  goToMemberInfo(event){
    this.router.navigate(['members/member/',event.target.id]);
  }

  ngOnDestroy() {
    // prevent memory leak when component is destroyed
    this.subscription.unsubscribe();
    this.subscriptionGym.unsubscribe();
    this.subscriptionMembers.unsubscribe();
   }

}

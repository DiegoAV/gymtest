import { GymService } from './../services/gym.service';
import { Router } from '@angular/router';
import { MemberService } from './../services/member.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {
  msubs: Subscription;
  gsubs: Subscription;
  membersList;
  gymList;
  constructor(private mService:MemberService, private gService:GymService,private router: Router) {
    this.membersList = [];
    this.gymList = [];
  }

  ngOnInit() {
    this.msubs = this.mService.getMembers().subscribe(res => {
      this.membersList = res;
    });

    this.gsubs = this.gService.getGyms().subscribe(res => {
      this.gymList = res;
    });
  }

  goToMemberInfo(memberId){
    console.log(memberId)
    this.router.navigate(['members/member/',memberId]);
  }

  getGymName(id){
    let gym = this.gymList.find(gym => gym.id == id);
    return gym? gym.name:false;
  }

  goToGym(gymId){
    this.router.navigate(['gym/info/',gymId]);
  }

  ngOnDestroy(){
    this.msubs.unsubscribe();
    this.gsubs.unsubscribe();
  }

}

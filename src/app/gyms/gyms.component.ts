import { GymService } from './../services/gym.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gyms',
  templateUrl: './gyms.component.html',
  styleUrls: ['./gyms.component.css']
})
export class GymsComponent implements OnInit {
  gymsList;
  constructor(private gymservice:GymService) {
    this.gymsList = [];
   }

  ngOnInit() {
    this.gymservice.getGyms().subscribe(res => {
      this.gymsList = res;
      console.log(this.gymsList)
    })
  }

  checkSession(){
    
    // sessionStorage.getItem('user');
  }

}

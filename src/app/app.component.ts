import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  hiddenLog;
  constructor(private router: Router){
    this.hiddenLog = true;
  }

  goGymList(){
    this.router.navigate(['gym']);
  }

  loginOut(){
    this.hiddenLog = true;
    this.router.navigate(['login']);
  }
}

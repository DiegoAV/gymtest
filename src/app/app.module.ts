import { MemberService } from './services/member.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpModule } from '@angular/http';
import { GymsComponent } from './gyms/gyms.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { RouterModule, Routes } from '@angular/router';

import { UsersService } from './services/users.service';
import { GymService } from './services/gym.service';
import { GymInfoComponent } from './gym-info/gym-info.component';
import { MembersComponent } from './members/members.component';
import { MemberInfoComponent } from './member-info/member-info.component';

const appRoutes : Routes = [
  {path:'login', component: LoginComponent},
  {path:'gym/info/:gymId', component: GymInfoComponent},
  {path:'gym', component: GymsComponent},
  {path:'members/member/:memberId', component: MemberInfoComponent},
  {path:'members', component: MembersComponent},
  {path:'', redirectTo:'/login', pathMatch:'full'},
  {path:'**', component: PagenotfoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    GymsComponent,
    PagenotfoundComponent,
    GymInfoComponent,
    MembersComponent,
    MemberInfoComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers:[
    UsersService,
    GymService,
    MemberService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

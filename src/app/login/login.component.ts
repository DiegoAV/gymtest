import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UsersService } from './../services/users.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  usersArr;
  @ViewChild('userId') userInput: ElementRef;
  @ViewChild('passwordId') passwordInput: ElementRef;
  userFound;
  constructor(private userService : UsersService, private router: Router) { 
    this.usersArr = [];
    this.userFound = false;
  }

  ngOnInit() { 
    this.userService.getUsers().subscribe(res => {
      this.usersArr = res;
    })
  }

  validateUser(){
    const user = this.userInput.nativeElement.value
    const password = this.passwordInput.nativeElement.value
   
    if(this.checkUser(user,password)){
      this.router.navigate(['gym']);
      // sessionStorage.setItem('user',user);
      // console.log(sessionStorage.getItem('user'))
      // sessionStorage.removeItem('user')
    }else{
      this.userFound = true;
    }
    // sessionStorage.getItem('key');
  }

  checkUser(user,password){
    let encontrado = false;
    this.usersArr.forEach(element => {
      if(user == element.user && password == element.password)
        encontrado = !encontrado;
    });
    return encontrado;
  }

}

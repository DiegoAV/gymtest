import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class GymService {

  constructor(private http:Http) { }

  getGyms() :  Observable<any[]>{
    return this.http.get('/assets/gyms.json')
    .map((res) => res.json())
    .catch(this.handleError);
  }

  getGym(gymId: number) : Observable<any[]>{
    return this.http.get('/assets/gyms.json')
    .map((res) => res.json())
    .map((data: Array<any>) => {
      return data.find(x => x.id == gymId)
    })
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  handleError(error: any){
    console.log(error);
    return Observable.throw(error);
  }
}

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class UsersService {

  constructor(private http:Http) { }

  getUsers() :  Observable<any[]>{
    return this.http.get('/assets/users.json')
    .map((res) => res.json())
    .catch(this.handleError);
  }

  handleError(error: any){
    console.log(error);
    return Observable.throw(error);
  }
}

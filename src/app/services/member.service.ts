import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MemberService {
  
  constructor(private http:Http) { }

  getMember(id: number) : Observable<any>{
    return this.http.get('/assets/members.json')
    .map((res) => res.json())
    .map((data: Array<any>) => {
      return data.find(member => member.id == id)
    }).catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    ;
  }

  getMembers() :  Observable<any[]>{
    return this.http.get('/assets/members.json')
    .map((res) => res.json())
    .catch(this.handleError);
  }

  getMembersOfGym(gymId: number) : Observable<any>{
    return this.http.get('/assets/members.json')
    .map((res) => res.json())
    .map((data: Array<any>) => {
      return data.filter(x => x.gymId == gymId)
    })
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  handleError(error: any){
    console.log(error);
    return Observable.throw(error);
  }
}
